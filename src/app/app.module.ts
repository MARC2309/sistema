import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/home/components/Categoryy/category/category.component';
import { CategoryEditComponent } from './components/home/components/Categoryy/category-edit/category-edit.component';
import { CategoriesComponent } from './components/home/components/Categoryy/categories/categories.component';
import { SuplierEditComponent } from './components/home/components/Supliers/suplier-edit/suplier-edit.component';
import { SuplierComponent } from './components/home/components/Supliers/suplier/suplier.component';
import { SuplierssComponent } from './components/home/components/Supliers/suplierss/suplierss.component';
import { ProductComponent } from './components/home/components/Productss/product/product.component';
import { ProductsComponent } from './components/home/components/Productss/products/products.component';
import { ProductEditComponent } from './components/home/components/Productss/product-edit/product-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CategoryComponent,
    CategoryEditComponent,
    CategoriesComponent,
    SuplierEditComponent,
    SuplierComponent,
    SuplierssComponent,
    ProductComponent,
    ProductsComponent,
    ProductEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
