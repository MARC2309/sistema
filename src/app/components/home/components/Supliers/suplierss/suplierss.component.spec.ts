import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuplierssComponent } from './suplierss.component';

describe('SuplierssComponent', () => {
  let component: SuplierssComponent;
  let fixture: ComponentFixture<SuplierssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuplierssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuplierssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
