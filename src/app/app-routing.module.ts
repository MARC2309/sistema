import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './components/home/components/Categoryy/categories/categories.component'
import { SuplierssComponent } from './components/home/components/Supliers/suplierss/suplierss.component'
import { ProductsComponent } from './components/home/components/Productss/products/products.component'

const routes: Routes = [
  {path:'categories',component: CategoriesComponent},
  {path:'suplierss',component:SuplierssComponent},
  {path:'products',component:ProductsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
